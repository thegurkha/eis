TARGET = kb
LIBS = -lm -lsqlite3 `pkg-config --cflags gtk+-3.0` `pkg-config --libs gtk+-3.0 gmodule-2.0`
CC = gcc
CFLAGS = -g -Wall `pkg-config --cflags gtk+-3.0`

.PHONY: default all clean

default: $(TARGET)
all: default

#OBJECTS = $(patsubst %.c, %.o, $(wildcard *.c))
OBJECTS = kb.o  gtk_functions.o
HEADERS = $(wildcard *.h)

%.o: %.c $(HEADERS)
	$(CC) $(CFLAGS) -c $< -o $@

.PRECIOUS: $(TARGET) $(OBJECTS)

$(TARGET): $(OBJECTS)
	$(CC) $(OBJECTS) -Wall $(LIBS) -o $@

clean:
	-rm -f *.o
	-rm -f $(TARGET)

