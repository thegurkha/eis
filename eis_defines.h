// EIS version
#define EIS_VERSION		"0.2.1"

// defines for the fields of the facts database table
#define OBJECT_FIELD		0
#define BINDING_FIELD		1
#define PROPERTY_FIELD	2

// define for the fields of the count(*) database table
#define COUNT_FIELD		0

// used to indicate 'search all levels of inheritance' in the queries
#define DEEP_SEARCH		999

// flag used to indicate whether a fact should override inherited facts of the same type
#define OVERRIDE_FLAG	'<'

// flag to show an inherited fact has been overriden by a newer fact
#define IGNORE_FACT		-1
#define USABLE_FACT		0

// flag to indicate congruence has been detected between two facts
#define CONGRUENT_FACT	1

// Breakpoint fact indicator
#define BREAKPOINT		-2

// conversation direction
#define DIR_IN			0
#define DIR_OUT			1
#define DIR_SYSTEM	2
#define DIR_BLANK		3
#define DIR_TRACE		4

// used to indicate which regions of the list we need normalize facts over.
// OBJECT_ONE isn't required at the moment. When there are only Object 1
// facts in the list we CAN use ALL_OBJECTS, but it is included because
// it makes logical sense for OBJECT_TWO to enumerate to 2 :)
#define ALL_OBJECTS		0
#define	OBJECT_ONE		1
#define OBJECT_TWO		2

// fact display formats
#define NATIVE_ORDER	0
#define FIRST_ORDER		1

// handy define to emit a blank line
#define BLANK_LINE		AppendText(EverythingUI, DIR_BLANK, "" )
#define BLANK_TRACE		AppendText(EverythingUI, DIR_TRACE, "" )

#ifndef ALREADY_DEFINED
#define ALREADY_DEFINED

#if defined MAIN

// database handle
// sqlite3 *knowledgeDB = NULL;

// global variables
int gDepth;
bool bTrace;

// structure used in linked list of facts
typedef struct Flink {
	char	*Object;
	char 	*Binding;
	char 	*Property;
	int		nDepth;
	int		nOverride;
	struct 	Flink *Next;
} Flink;

// pointers to the start and end of the facts linked list
struct Flink *Root, *End, *Breakpoint;

// User Interface structure
typedef struct EIS_GUI_Elements {

	// main window
	GtkWidget	*MainWnd;
	GtkEntry	*QueryField;
	GtkWidget	*TextView;

	// system trace window
	GtkWidget	*TraceWnd;
	GtkWidget	*TraceView;

} GUI_Elements;

GUI_Elements EIS_Gui;

// structure used in linked list of command history
typedef struct Clink {

	char *UserInput;				// user input
	int nCommandID;					// position of this command in the history list

	struct Clink *Next;
} Clink;

// pointers to the start and end of the command history linked list
struct Clink *cRoot, *cEnd;

// ######################################################################################
#else
// ######################################################################################

// database handle
// extern sqlite3 *knowledgeDB;

// global variables
extern int gDepth;
extern bool bTrace;

// structure used in linked list of facts
typedef struct Flink {
	char	*Object;
	char 	*Binding;
	char 	*Property;
	int		nDepth;
	int		nOverride;
	struct 	Flink *Next;
} Flink;

// pointers to the start and end of the linked list
extern Flink *Root, *End, *Breakpoint;

// structure used in linked list of command history
typedef struct Clink {

	char *UserInput;				// user input
	int nCommandID;					// position of this command in the history list

	struct Clink *Next;
} Clink;

// structure used in linked list of command history
extern Clink *cRoot, *cEnd;

// User Interface structure
typedef struct EIS_GUI_Elements {

	// main window
	GtkWidget	*MainWnd;
	GtkEntry	*QueryField;
	GtkWidget	*TextView;

	// system trace window
	GtkWidget	*TraceWnd;
	GtkWidget	*TraceView;

} GUI_Elements;

extern GUI_Elements EIS_Gui;

#endif 	// MAIN

#endif // ALREADY_DEFINED
