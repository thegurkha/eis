/*
 * gtk_functions.c
 *
 * Copyright 2013-23 Dave McKay dave@dpocompliance.co.uk
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 *
 */

#include <gtk/gtk.h>

#include <string.h>
#include <ctype.h>
#include <stdio.h>
#include <time.h>
#include <stdlib.h>
#include <string.h>
#include <stdbool.h>
#include <time.h>

#include <sqlite3.h>

#include "eis_defines.h"
#include "function_headers.h"

extern sqlite3 *knowledgeDB;

G_MODULE_EXPORT void on_window_destroy(GObject *object, GUI_Elements *EverythingUI)
{
	// quit the GUI loop
	gtk_main_quit();

}	// end of on_window_destroy

G_MODULE_EXPORT void on_text_field_activate(GObject *object, GUI_Elements *EverythingUI)
{
	// user hit enter, let's process their input
	const gchar *QueryString;

	// user has hit the post button or the enter key, retrieve the text from the text entry field
	QueryString = gtk_entry_get_text(GTK_ENTRY(EverythingUI->QueryField));

	// show it in both windows
	AppendText(EverythingUI, DIR_IN, (char *) QueryString);
	AppendText(EverythingUI, DIR_TRACE, (char *) QueryString);

	// act upon their input
	ParseUserInput((char *) QueryString, EverythingUI);

	// clear out the text entry field buffer ready for their next input
	gtk_entry_set_text(GTK_ENTRY(EverythingUI->QueryField), "");

}	// end of on_text_field_button_press_event
