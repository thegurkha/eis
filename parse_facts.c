/*
 * parse_facts.c
 *
 * Copyright 2013 Dave McKay dmckay@btconnect.com
 *
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <ctype.h>

// structure used in linked list of facts
struct Tlink {
	char *Fact;
	int nRecipcrocal;

	struct Tlink *Next;
};

// pointers to the start and end of the linked list
struct Tlink *Root, *End;

int ReadInFactsFile( void );
struct Tlink *AddToList(char *Content, int nReciprocity);
void DoListWalk( void );

void WriteOutDumpFile( void );
void WriteOutFacts( void );

#define NULL_BYTE					0x00
#define RETURN						0x0D
#define NEW_LINE					0x0A
#define APOSTROPHE				0x27
#define EXCLAMATION 			0x21

#define RECIPROCAL 				1
#define NORMAL			 			0

#define HASH							'#'
#define SPACE							' '

char *Line1="PRAGMA foreign_keys=OFF;";
char *Line2="BEGIN TRANSACTION;";
char *LineLast="COMMIT;";

char *CreateFactsTable="CREATE TABLE \"facts\" (\n\t\"object\" TEXT,\n\t\"binding\" TEXT,\n\t\"property\" TEXT\n);\n";

// SQL output file handle
FILE *OutFile;

int main(int argc, char **argv)
{
	ReadInFactsFile();
	// DoListWalk();
	WriteOutDumpFile();

	return (0);

}	// end of main

int ReadInFactsFile( void )
{
	char szBuffer[ 1024 ];
	int FirstCharacter;
	int nResponse=0;

	// file handle
	FILE *InFile;

	// open the file
	InFile=fopen("facts.txt", "rt");

	// did it open OK
	if (InFile == NULL) {
		printf("Can't open facts.txt file.\n");
		return (1);
	}

    // read it line by line until we get to the end of the file
    while (fgets(szBuffer, 1023, InFile) != NULL) {

		// the first character of the line dictates what kind of line it is
		FirstCharacter = szBuffer[0];

		// if it is a comment ignore it
		if (FirstCharacter == HASH)
				continue;

		if (FirstCharacter == SPACE)
				continue;

		if (FirstCharacter == RETURN)
				continue;

		if (FirstCharacter == EXCLAMATION) {
			// add this line to the linked list for later processing
			// this is a fact with a reciprocal manufactured// eg. dave knows eric, then eric knows dave
			AddToList(szBuffer+1, RECIPROCAL);

		}
		else {
			// add this line to the linked list for later processing
			// normal fact, no reciprocity
			AddToList(szBuffer, NORMAL);
		}
	}

	// close the file
	fclose( InFile );

	return ( nResponse );

}   // ReadInPhrasesFile

struct Tlink *AddToList(char *Content, int nReciprocity)
{
	struct Tlink *fPtr;

	// no facts in the list yet
	if (Root == NULL) {

		// get memory for the new fact structure
		fPtr = (struct Tlink *) malloc(sizeof(struct Tlink));

		// can't allocate memory
		if (fPtr == NULL) {
			printf("Error: Can't allocate memory for fact: %s in linked list.", Content);
			return (NULL);
		}

		// set this fact as the start of the linked list
		Root = End = fPtr;
	}
	// facts exist, append new fact to the list
	else {
		// get memory for the new fact structure
		fPtr = (struct Tlink *) malloc(sizeof(struct Tlink));

		// can't allocate memory
		if (fPtr == NULL) {
			printf("Error: Can't allocate memory for fact: %s in linked list.", Content);
			return (NULL);
		}

		// point the hitherto last topic to the new last topic
		End->Next=fPtr;

		// this is now the end of the linked list
		End=fPtr;
	}

	// populate the fields

	fPtr->Fact = strdup( Content );

	// can't allocate memory
	if (fPtr->Fact == NULL) {
		printf("Error: Can't allocate memory for fact: %s in linked list.", Content);
		return (NULL);
	}

	fPtr->nRecipcrocal = nReciprocity;

	// return from function
	return (fPtr);

}	// end of AddToList

void DoListWalk( void )
{
	struct Tlink *fPtr;

	// From the start of the linked list (root), display all topics
	for (fPtr=Root; fPtr != NULL; fPtr=fPtr->Next) {

		printf("'%s'\n", fPtr->Fact );

	}

}	// end of DoListWalk

void WriteOutDumpFile( void )
{
	// open the file
	OutFile=fopen("KnowledgeBase.sql", "w");

	// did it open OK
	if (OutFile == NULL) {
		printf("Can't open SQL dump file.\n");
		return;
	}

   fprintf(OutFile, "%s\n", Line1 );
   fprintf(OutFile, "%s\n\n", Line2 );
   fprintf(OutFile, "%s\n", CreateFactsTable );

	// Each of these functions runs through the linked list looking for entries of a certain type
	// any that are found are written to OutFile as a SQL statement to populate the database
	WriteOutFacts();

	// add the final COMMIT line
	fprintf(OutFile, "%s\n", LineLast );

	// close the OutFile
	fclose( OutFile );

}	// end of WriteOutDumpFile

void WriteOutFacts( void )
{
	struct Tlink *fPtr;
	char szSeps[]=";\n";
	char *Object, *Binding, *Property;

	// From the start of the linked list (root), look for response entries
	for (fPtr=Root; fPtr != NULL; fPtr=fPtr->Next) {

		Object = strtok(fPtr->Fact, szSeps);
		Binding = strtok(NULL, szSeps);
		Property = strtok(NULL, szSeps);

		printf("INSERT INTO \"facts\" VALUES('%s','%s','%s');\n", Object, Binding, Property );
		fprintf(OutFile, "INSERT INTO \"facts\" VALUES('%s','%s','%s');\n", Object, Binding, Property );

		if (fPtr->nRecipcrocal == RECIPROCAL) {
			printf("Reciprocal: INSERT INTO \"facts\" VALUES('%s','%s','%s');\n", Property, Binding, Object );
			printf("INSERT INTO \"facts\" VALUES('%s','%s','%s');\n", Property, Binding, Object );
			fprintf(OutFile, "INSERT INTO \"facts\" VALUES('%s','%s','%s');\n", Property, Binding, Object );
		}
	}

	fprintf(OutFile, "\n");

}	// end of WriteOutPhrases
