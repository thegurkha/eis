/*
 * add_facts.c
 * 
 * Copyright 2013 Dave McKay dmckay@btconnect.com
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 * 
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <ctype.h>

#include <sqlite3.h>

// database handle
sqlite3 *knowledgeDB = NULL;

int ReadInFactsFile( void );
int PostFact(char *Response);

const char INSERT_FACT[] = "INSERT INTO facts VALUES ('%s', '%s', '%s')";

int main(int argc, char **argv)
{
	
	char *pError;
	int nResponse;
			
	// intialise the SQLite library
	sqlite3_initialize( );
	
	// open the database
	nResponse = sqlite3_open_v2( "KnowledgeBase.sl3", &knowledgeDB, SQLITE_OPEN_READWRITE, NULL );
	
	// if it didn't open OK, shutdown and exit
	if ( nResponse != SQLITE_OK) {
		printf("%s Couldn't open KnowledgeBase database.\n", argv[0]);
		sqlite3_close( knowledgeDB );
		exit (-1);
	}
	else
			printf("Opened KnowledgeBase database OK\n");
			
	nResponse = sqlite3_exec(knowledgeDB, "DELETE FROM facts", NULL, NULL, &pError);
	
	if ( nResponse != SQLITE_OK ) {
		printf("Couldn't delete old facts.\n");
		return (-1);
	}
	else
			printf("Zapped old facts.\n");
	
	ReadInFactsFile();
	
	// shut down and exit	
	sqlite3_close( knowledgeDB );
	sqlite3_shutdown( );
	
	return (0);
	
}	// end of main

int ReadInFactsFile( void )
{
	char szBuffer[ 1024 ];
	int nResponse=0;

	// file handle
	FILE *InFile;

	// open the file
	InFile=fopen("facts.txt", "rt");

	// did it open OK
	if (InFile == NULL) {
		printf("Can't open facts file.\n");
		return (1);
	}

	// read it line by line until we get to the end of the file
	while (fgets(szBuffer, 1023, InFile) != NULL) {
		
		if (szBuffer[0] == '#')
				continue;
		
		// add the fact to the knowledge base
		PostFact(szBuffer);	
	}

	return ( nResponse );

}   // ReadInPhrasesFile

int PostFact(char *FactString)
{
	char *Ptr, szSeps[]=";\n";
	char szObject[ 50 ], szBinding[ 50 ], szProperty[ 50 ];
	char szSQLStatement[1024];
	char *pError;
	
	int nResponse;
	
	// extract the object 
	Ptr = strtok(FactString, szSeps);
	
	// if we got it OK, save it for later
	if (Ptr != NULL)
		sprintf(szObject, "%s", Ptr);
	else {
		// flag error
		printf("Can't extract Object from fact phrase\n");
		return (-1);
	}
	
	// extract the binding
	Ptr = strtok(NULL, szSeps);
	
	// if we got it ok, save it for later
	if (Ptr != NULL)
		sprintf(szBinding, "%s", Ptr);
	else {
		// flag error
		printf("Can't extract Binding from fact phrase\n");
		return (-1);
	}
	
	// extract the property
	Ptr = strtok(NULL, szSeps);
	
	// if we got it ok, save it for later
	if (Ptr != NULL)
		sprintf(szProperty, "%s", Ptr);
	else {
		// flag error
		printf("Can't extract Property from fact phrase\n");
		return (-1);
	}
	
	// compose the SQL string
	sprintf(szSQLStatement, INSERT_FACT, szObject, szBinding, szProperty);
	
	puts( szSQLStatement );
	
	nResponse = sqlite3_exec(knowledgeDB, szSQLStatement, NULL, NULL, &pError);
	
	if ( nResponse != SQLITE_OK ) {
		printf("Couldn't add that fact.\n");
		return (-1);
	}
	
	return (0);
	
}	// end of PostFact

