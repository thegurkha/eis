/*
 * function_headers.h
 *
  * Copyright 2013-23 Dave McKay dave@dpocompliance.co.uk
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 *
 */
#include "eis_defines.h"

void PerformObjectQuery(char *pSearchClue, int nLimit, GUI_Elements *EverythingUI);
void ObjectQuery(char *pSearchClue, int nDepth, int nLimit, GUI_Elements *EverythingUI);

void PerformPropertyQuery(char *pSearchClue, int nLimit, GUI_Elements *EverythingUI);
void PropertyQuery(char *pSearchClue, char *MasterBinding, int nDepth, int nLimit, GUI_Elements *EverythingUI);
int CheckForOverrides(char *pObject, char *pBinding, GUI_Elements *EverythingUI);

void PerformBindingQuery(char *pSearchClue, GUI_Elements *EverythingUI);
void BindingQuery(char *pSearchClue, GUI_Elements *EverythingUI);

void PerformLinkQuery(char *pSearchClueObject, char *pSearchClueProperty, int nLimit, GUI_Elements *EverythingUI);
int DoLinkQueryListWalk( GUI_Elements *EverythingUI, char *pSearchClue);

void PerformConfluxQuery(char *pSearchClueObject1, char *pSearchClueObject2, GUI_Elements *EverythingUI);
bool CongruenceCheck(char *pSearchClueObject1, char *pSearchClueObject2, struct Flink *fPtr1, struct Flink *fPtr2, GUI_Elements *EverythingUI);

void ParseUserInput(char *QueryString, GUI_Elements *EverythingUI);

struct Flink *AddFactToList(int nDepth, char *pObject, char *pBinding, char *pProperty, GUI_Elements *EverythingUI);
void DoListWalk( GUI_Elements *EverythingUI);
void DeleteAllFacts(void);

void NormalizeFacts(int nScope, GUI_Elements *EverythingUI);
void OverrideInheritedFacts(int nScope, GUI_Elements *EverythingUI, struct Flink *fPtr);

int CheckForNeedlessRecursion( GUI_Elements *EverythingUI, char *pProperty, int nDepth);

void AppendText( GUI_Elements *EverythingUI, int nDirection, char *NewString, ...);
void WriteOutFile( GUI_Elements *EverythingUI);
void PerformAboutCommand( GUI_Elements *EverythingUI);

struct Clink *AddCommandToList(char *pUserInput);
void PerformHistoryWalk( GUI_Elements *EverythingUI);
void PerformRedoCommand( GUI_Elements *EverythingUI, int nCommand );

void ListAllObjects(int nField, GUI_Elements *EverythingUI);
void ListFacts( GUI_Elements *EverythingUI, int nFactOrder);
void CountFacts( GUI_Elements *EverythingUI);

G_MODULE_EXPORT void on_window_destroy(GObject *object, GUI_Elements *EverythingUI);
G_MODULE_EXPORT void on_text_field_activate(GObject *object, GUI_Elements *EverythingUI);
