/*
 * kb.c
 *
 * Copyright 2013-23 Dave McKay dave@dpocompliance.co.uk
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 *
 */

#include <gtk/gtk.h>

#include <string.h>
#include <ctype.h>
#include <stdio.h>
#include <time.h>
#include <stdlib.h>
#include <string.h>
#include <stdbool.h>
#include <time.h>

#include <sqlite3.h>

#define MAIN
#include "eis_defines.h"

#include "function_headers.h"
#include "eis_commands.h"

	sqlite3 *knowledgeDB = NULL;

int main(int argc, char **argv)
{
	int nResponse;

	GtkTextBuffer	*buffer;
	GtkBuilder 		*builder;
	GUI_Elements	*EverythingUI;

	char szDatabaseName[]="KnowledgeBase.sl3";	// name of the facts database

	// allocate the memory needed for our structure
	EverythingUI = g_slice_new(GUI_Elements);

	// init the gtk toolkit
	gtk_init(&argc, &argv);

	// get a new interface builder
	builder = gtk_builder_new();

	// read the GUI definitions from the XML file
	gtk_builder_add_from_file(builder, "eis.glade", NULL);

	// point the structure members to the UI elements we need to control
	// main window
	EverythingUI->MainWnd = GTK_WIDGET (gtk_builder_get_object(builder, "MainWindow"));
	EverythingUI->QueryField = GTK_ENTRY (gtk_builder_get_object(builder, "QueryTextField"));
	EverythingUI->TextView = GTK_WIDGET (gtk_builder_get_object(builder, "TextView"));

	// trace window
	EverythingUI->TraceWnd = GTK_WIDGET (gtk_builder_get_object(builder, "TraceWindow"));
	EverythingUI->TraceView = GTK_WIDGET (gtk_builder_get_object(builder, "TraceView"));

	// connect the signal handlers
	gtk_builder_connect_signals(builder, EverythingUI);

	// release the builder memory
	g_object_unref(G_OBJECT (builder));

	// get the style settings from the CSS file
	GtkCssProvider *cssProvider = gtk_css_provider_new();
  gtk_css_provider_load_from_path(cssProvider, "themes.css", NULL);
  gtk_style_context_add_provider_for_screen(gdk_screen_get_default(), GTK_STYLE_PROVIDER(cssProvider), GTK_STYLE_PROVIDER_PRIORITY_USER);

	// get a handle on the buffer from the main text window
	buffer = gtk_text_view_get_buffer(GTK_TEXT_VIEW(EverythingUI->TextView));

	// create some main window style tags
	gtk_text_buffer_create_tag(buffer, "italic", "style", PANGO_STYLE_ITALIC, NULL);
	gtk_text_buffer_create_tag(buffer, "bold", "weight", PANGO_WEIGHT_BOLD, NULL);

	// get a handle on the buffer from the trace text window
	buffer = gtk_text_view_get_buffer(GTK_TEXT_VIEW (EverythingUI->TraceView));

	// create some main window style tags
	gtk_text_buffer_create_tag(buffer, "italic", "style", PANGO_STYLE_ITALIC, NULL);
	gtk_text_buffer_create_tag(buffer, "bold", "weight", PANGO_WEIGHT_BOLD, NULL);

	// intialise the SQLite library
	sqlite3_initialize();

	// no facts yet
	Root = End = NULL;

	// open the database
	nResponse = sqlite3_open_v2(szDatabaseName, &knowledgeDB, SQLITE_OPEN_READWRITE, NULL);

	// if it didn't open OK, shutdown and exit
	if (nResponse != SQLITE_OK) {
		printf("%s Couldn't open KnowledgeBase database.\n", argv[0]);
		sqlite3_close(knowledgeDB);
		exit (-1);
	}

	// set the default values for some global settings
	gDepth=5;
	bTrace=false;

	// announce ourselves
	AppendText(EverythingUI, DIR_TRACE, "EIS %s :: Compiled on %s at %s.", EIS_VERSION, __DATE__, __TIME__);
	AppendText(EverythingUI, DIR_TRACE, "DB: %s", szDatabaseName);

	// display the main window
	gtk_widget_show(EverythingUI->MainWnd);

	// display the trace window
	gtk_widget_show(EverythingUI->TraceWnd);

	// enter the UI event handler loop
	gtk_main();

	// shut down sqlite3
	sqlite3_close(knowledgeDB);
	sqlite3_shutdown();

	// and exit
	return (0);

}	// end of main

void PerformObjectQuery(char *pSearchClue, int nLimit, GUI_Elements *EverythingUI)
{
	// empty the linked list
	DeleteAllFacts();

	// perform the query
	ObjectQuery(pSearchClue, 0, nLimit, EverythingUI);

	// make sure we don't list anything that isn't validly related to the other data
	NormalizeFacts(ALL_OBJECTS, EverythingUI);

	// display the facts
	DoListWalk(EverythingUI);

}	// end of PerformObjectQuery

void ObjectQuery(char *pSearchClue, int nDepth, int nLimit, GUI_Elements*EverythingUI)
{
	char szTemplate[]="SELECT * FROM facts WHERE object=\"%s\"";
	char szSQLString[100]="";
	char *pObject, *pBinding, *pProperty;
	int nResponse;
	sqlite3_stmt *stmt = NULL;

	// create the SQL string
	sprintf(szSQLString, szTemplate, pSearchClue);
	// AppendText(EverythingUI, DIR_TRACE, "%s", szSQLString);

	// prepare the SQL statement
	nResponse = sqlite3_prepare_v2(knowledgeDB, szSQLString, -1, &stmt, NULL);

	// if not OK, bomb out
	if (nResponse != SQLITE_OK)
			return;

	// if OK, start to walk through the record set
	nResponse = sqlite3_step(stmt);

	while (nResponse == SQLITE_ROW) {

		// take a copy of the three elements of the fact
		pObject = (char*) sqlite3_column_text(stmt, OBJECT_FIELD);
		pBinding = (char*) sqlite3_column_text(stmt, BINDING_FIELD);
		pProperty = (char*) sqlite3_column_text(stmt, PROPERTY_FIELD);

		if (AddFactToList(nDepth, pObject, pBinding, pProperty, EverythingUI) == NULL) {

			AppendText(EverythingUI, DIR_SYSTEM, "Couldn't add fact: %s %s %s", pObject, pBinding, pProperty);
			return;
		}

		// search for data in related facts, but only if we don't cause needless recursion
		//
		if ((nDepth+1) <= nLimit) {

			if (CheckForNeedlessRecursion(EverythingUI, pProperty, nDepth) != IGNORE_FACT)
					ObjectQuery(pProperty, nDepth+1, nLimit, EverythingUI);
		}

		// check for the next record
		nResponse = sqlite3_step(stmt);
	}

	// close off the sql statement
	sqlite3_finalize(stmt);

}	// end of ObjectQuery

void PerformPropertyQuery(char *pSearchClue, int nLimit, GUI_Elements*EverythingUI)
{
	// empty the linked list
	DeleteAllFacts();

	// perform the query, note that PropertyQuery is called differently later on.
	// The original pSearchClue is used as the second parameter for recursive calls so
	// that the binding from each 0 depth matching record can be referenced at deeper levels.
	PropertyQuery(pSearchClue, "", 0, nLimit, EverythingUI);

	// display the facts
	DoListWalk(EverythingUI);

}	// end of PerformPropertyQuery

void PropertyQuery(char *pSearchClue, char *MasterBinding, int nDepth, int nLimit, GUI_Elements*EverythingUI)
{
	char szTemplate[]="SELECT * FROM facts WHERE property=\"%s\"";
	char szSQLString[100]="";
	char *pObject, *pBinding, *pProperty;
	int nResponse;
	sqlite3_stmt *stmt = NULL;

	struct Flink *fPtr;

	// create the SQL string
	sprintf(szSQLString, szTemplate, pSearchClue);
	// AppendText(EverythingUI, DIR_TRACE, "%s", szSQLString);

	// prepare the SQL statement
	nResponse = sqlite3_prepare_v2(knowledgeDB, szSQLString, -1, &stmt, NULL);

	// if not OK, bomb out
	if (nResponse != SQLITE_OK)
			return;

	// if OK, start to walk through the record set
	nResponse = sqlite3_step(stmt);

	// walk through the record set
	while (nResponse == SQLITE_ROW) {

		// take a copy of the three elements of the fact
		pObject = (char*) sqlite3_column_text(stmt, OBJECT_FIELD);
		pBinding = (char*) sqlite3_column_text(stmt, BINDING_FIELD);
		pProperty = (char*) sqlite3_column_text(stmt, PROPERTY_FIELD);

		// take a record of the orignal binding from the depth 0 match
		MasterBinding = (nDepth == 0) ? pBinding : MasterBinding;

		// are we at nesting depth 0?
		if (nDepth != 0) {

			// check that there are no conflicting facts about the newly found object
			nResponse = CheckForOverrides(pObject, MasterBinding, EverythingUI);
		}

		// add the fact to the list
		fPtr = AddFactToList(nDepth, pObject, pBinding, pProperty, EverythingUI);

		// did it add OK
		if (fPtr == NULL) {
			AppendText(EverythingUI, DIR_SYSTEM, "Couldn't add fact: %s %s %s", pObject, pBinding, pProperty);
			return;
		}

		// do we ignore this fact (because a conflicting fact was found)?
		fPtr->nOverride = (nResponse == 1) ? IGNORE_FACT : 0;

		// if this object has no conflicting facts, search for descendants of it
		if (nResponse != 1) {

			// search for data in related facts
			if ((nDepth+1) <= nLimit) {

				// search for the object, pass in the original MasterBinding and the pointer to the new fact
				PropertyQuery(pObject, MasterBinding, nDepth+1, nLimit, EverythingUI);
			}
		}

		// try to move to the next record in the record set
		nResponse = sqlite3_step(stmt);
	}

	sqlite3_finalize(stmt);

}	// end of PropertyQuery

void PerformLinkQuery(char *pSearchClueObject, char *pSearchClueProperty, int nLimit, GUI_Elements *EverythingUI)
{
	int nFound = 0;

	// empty the linked list
	DeleteAllFacts();

	// perform the query
	ObjectQuery(pSearchClueObject, 0, nLimit, EverythingUI);

	// make sure we don't list anything that isn't validly related to the other data
	NormalizeFacts(ALL_OBJECTS, EverythingUI);

	// search for a fact with the required target property
	nFound = DoLinkQueryListWalk(EverythingUI, pSearchClueProperty);

	if (nFound == 0)
			AppendText(EverythingUI, DIR_OUT, "No link found between: %s and %s", pSearchClueObject, pSearchClueProperty);

}	// end of PerformLinkQuery

int DoLinkQueryListWalk(GUI_Elements *EverythingUI, char *pSearchClue)
{
	struct Flink *fPtr;
	int nFound = 0;

	// From the start of the linked list (root), search for a fact with the target property
	for (fPtr=Root; fPtr != NULL; fPtr=fPtr->Next) {

		if ((fPtr->nOverride > IGNORE_FACT) && (strcasecmp(pSearchClue, fPtr->Property) == 0)) {

			// matching fact has been found
			nFound++;

			// if we are not at depth 0 then there must be other lower level facts
			// that lead to this fact being found here. Recursively seek them, by looking
			// for facts with the object of this fact as the property of other facts, at lower
			// levels of abstraction (closer in the tree to the root object)
			if (fPtr->nDepth > 0)
					DoLinkQueryListWalk(EverythingUI, fPtr->Object);

			// let them know what we found
			AppendText(EverythingUI, DIR_OUT, "%*s%s %s %s [%d]", fPtr->nDepth+1, "", fPtr->Object, fPtr->Binding, fPtr->Property, fPtr->nDepth);
		}
	}

	return (nFound);

}	// end of DoLinkQueryListWalk

void PerformConfluxQuery(char *pSearchClueObject1, char *pSearchClueObject2, GUI_Elements *EverythingUI)
{
	struct Flink *fPtr1, *fPtr2;
	//
	// how much confluence is there between two objects
	// (in other words, what degree of similarity do they have)
	//

	// empty the linked list
	DeleteAllFacts();

	// search for all facts regarding the first object
	ObjectQuery(pSearchClueObject1, 0, DEEP_SEARCH, EverythingUI);

	// rationalize the object 1 facts with respect to over-riders
	NormalizeFacts(ALL_OBJECTS, EverythingUI);

	// add a breakpoint fact
	Breakpoint = AddFactToList(BREAKPOINT, "-Breakpoint-", "", "", EverythingUI);

	if (Breakpoint == NULL) {

		// something went wrong - can't complete the comparison
		AppendText(EverythingUI, DIR_SYSTEM, "Memory allocation failure adding breakpoint fact");
		return;
	}

	// without clearing out the fact list, search for all facts regarding
	// the second object and append them to the facts regarding object1
	// so that we have both sets of facts for comparison in the same linked list
	ObjectQuery(pSearchClueObject2, 0, DEEP_SEARCH, EverythingUI);

	// rationalize the object 2 facts with respect to over-riders
	NormalizeFacts(OBJECT_TWO, EverythingUI);

	// xxx don't need this here it is just for testing - remove later
	DoListWalk(EverythingUI);

	// From the start of the linked list (root) check the object & property of object 1 against object 2
	for (fPtr1=Root; fPtr1 != Breakpoint; fPtr1=fPtr1->Next) {

		// if this object 1 fact is not over-ridden and is not already tagged as CONGRUENT
		if ((fPtr1->nOverride > IGNORE_FACT) && (fPtr1->nOverride != CONGRUENT_FACT)) {

			// compare it against the next object 2 fact that is not over-ridden and is not already tagged as CONGRUENT
			for (fPtr2=Breakpoint->Next; fPtr2 != NULL; fPtr2=fPtr2->Next) {

				if ((fPtr2->nOverride > IGNORE_FACT)  && (fPtr2->nOverride != CONGRUENT_FACT)) {

					// if there is congruence xxx decide what to do here
					if (CongruenceCheck(pSearchClueObject1, pSearchClueObject2, fPtr1, fPtr2, EverythingUI) == TRUE) {

						// fPtr1->nOverride = CONGRUENT_FACT;
						// fPtr2->nOverride = CONGRUENT_FACT;
						// AppendText(EverythingUI, DIR_OUT, "%*s%s %s %s [%d, %s]", fPtr1->nDepth+1, "", fPtr1->Object, fPtr1->Binding, fPtr1->Property, fPtr1->nDepth, pSearchClueObject1);
						// AppendText(EverythingUI, DIR_OUT, "%*s%s %s %s [%d, %s]", fPtr2->nDepth+1, "", fPtr2->Object, fPtr2->Binding, fPtr2->Property, fPtr2->nDepth, pSearchClueObject2);
					}
				}
			}
		}
	}

}	// end of PerformConfluxQuery

bool CongruenceCheck(char *pSearchClueObject1, char *pSearchClueObject2, struct Flink *fPtr1, struct Flink *fPtr2, GUI_Elements *EverythingUI)
{
	bool bResult =FALSE;

	if (strcasecmp(fPtr1->Object, fPtr2->Object) == 0)
			bResult  = TRUE;

	else if (strcasecmp(fPtr1->Object, fPtr2->Property) == 0)
			bResult  = TRUE;

	else if (strcasecmp(fPtr1->Property, fPtr2->Object) == 0)
			bResult  = TRUE;

	else if (strcasecmp(fPtr1->Property, fPtr2->Property) == 0)
			bResult  = TRUE;

	else
			bResult  = FALSE;

	if (bResult) {

//		AppendText(EverythingUI, DIR_OUT, "%*s%s %s %s [%d, %s]", fPtr1->nDepth+1, "", fPtr1->Object, fPtr1->Binding, fPtr1->Property, fPtr1->nDepth, pSearchClueObject1);
//		AppendText(EverythingUI, DIR_OUT, "%*s%s %s %s [%d, %s]", fPtr2->nDepth+1, "", fPtr2->Object, fPtr2->Binding, fPtr2->Property, fPtr2->nDepth, pSearchClueObject2);
//		AppendText(EverythingUI, DIR_OUT, "----");
	}

	return (bResult);

}	// end of CongruenceCheck

void PerformBindingQuery(char *pSearchClue, GUI_Elements *EverythingUI)
{
	// empty the linked list
	DeleteAllFacts();

	// perform the query
	BindingQuery(pSearchClue, EverythingUI);

	// display the facts
	DoListWalk(EverythingUI);

}	// end of PerformBindingQuery

void BindingQuery(char *pSearchClue, GUI_Elements *EverythingUI)
{
	char szTemplate[]="SELECT * FROM facts WHERE binding=\"%s\" OR binding=\"<%s\"";
	char szSQLString[150]="";
	char *pObject, *pBinding, *pProperty;
	int nResponse;
	sqlite3_stmt *stmt = NULL;

	struct Flink *fPtr;

	// create the SQL string
	sprintf(szSQLString, szTemplate, pSearchClue, pSearchClue);
	// AppendText(EverythingUI, DIR_TRACE, "%s", szSQLString);

	// prepare the SQL statement
	nResponse = sqlite3_prepare_v2(knowledgeDB, szSQLString, -1, &stmt, NULL);

	// if not OK, bomb out
	if (nResponse != SQLITE_OK)
			return;

	// if OK, start to walk through the record set
	nResponse = sqlite3_step(stmt);

	// walk through the record set
	while (nResponse == SQLITE_ROW) {

		// take a copy of the three elements of the fact
		pObject = (char*) sqlite3_column_text(stmt, OBJECT_FIELD);
		pBinding = (char*) sqlite3_column_text(stmt, BINDING_FIELD);
		pProperty = (char*) sqlite3_column_text(stmt, PROPERTY_FIELD);

		// add the fact to the list
 		fPtr = AddFactToList(0, pObject, pBinding, pProperty, EverythingUI);

		// did it add OK
		if (fPtr == NULL) {
			AppendText(EverythingUI, DIR_SYSTEM, "Couldn't add fact: %s %s %s", pObject, pBinding, pProperty);
			return;
		}

		// try to move to the next record in the record set
		nResponse = sqlite3_step(stmt);
	}

	sqlite3_finalize(stmt);

}	// BindingQuery

int CheckForOverrides(char *pObject, char *pBinding, GUI_Elements *EverythingUI)
{
	char szTemplate[]="SELECT * FROM facts WHERE object=\"%s\" AND binding=\"<%s\"";
	char szSQLString[200]="";
	char *pcObject, *pcBinding, *pcProperty;
	int nResponse;
	sqlite3_stmt *stmt = NULL;

	// create the SQL string
	sprintf(szSQLString, szTemplate, pObject, pBinding);
	// AppendText(EverythingUI, DIR_TRACE, "%s", szSQLString);

	// prepare the SQL statement
	nResponse = sqlite3_prepare_v2(knowledgeDB, szSQLString, -1, &stmt, NULL);

	// if not OK, bomb out
	if (nResponse != SQLITE_OK)
			return (1);

	// if OK, start to walk through the record set
	nResponse = sqlite3_step(stmt);

	if (nResponse == SQLITE_ROW) {

		// if there is even one fact found, we have a conflict
		nResponse = 1;

		// take a copy of the three elements of the fact
		pcObject = (char*) sqlite3_column_text(stmt, OBJECT_FIELD);
		pcBinding = (char*) sqlite3_column_text(stmt, BINDING_FIELD);
		pcProperty = (char*) sqlite3_column_text(stmt, PROPERTY_FIELD);

		AppendText(EverythingUI, DIR_TRACE, "** Conflicting -> %s %s %s", pcObject, (pcBinding[0] == OVERRIDE_FLAG) ? pcBinding+1 : pcBinding, pcProperty);
	}
	else
			nResponse = 0;

	return (nResponse);

}	// end of CheckForOverrides

struct Flink *AddFactToList(int nDepth, char *pObject, char *pBinding, char *pProperty, GUI_Elements *EverythingUI)
{
	struct Flink *fPtr;

	// no facts in the list yet
	if (Root == NULL) {

		// get memory for the new fact structure
		fPtr = (struct Flink *) malloc(sizeof(struct Flink));

		// can't allocate memory
		if (fPtr == NULL) {
			AppendText(EverythingUI, DIR_SYSTEM, "Error: Can't allocate memory for fact: %s in linked list.\n", pObject);
			return (NULL);
		}

		// set this fact as the start of the linked list
		Root=End=fPtr;
	}
	// facts exist, append new fact to the list
	else {
		// get memory for the new fact structure
		fPtr = (struct Flink *) malloc(sizeof(struct Flink));

		// can't allocate memory
		if (fPtr == NULL) {
			AppendText(EverythingUI, DIR_SYSTEM, "Error: Can't allocate memory for faact: %s in linked list.\n", pObject);
			return (NULL);
		}

		// point the hitherto last word to the new last word
		End->Next=fPtr;

		// this is now the end of the linked list
		End=fPtr;
	}

	// populate the object field
	fPtr->Object=strdup(pObject);

	// can't allocate memory
	if (fPtr->Object == NULL) {
		AppendText(EverythingUI, DIR_SYSTEM, "Error: Can't allocate memory for fPtr->Object for %s.\n", pObject);
		return (NULL);
	}

	// populate the binding field
	if (pBinding[0] == OVERRIDE_FLAG) {
		fPtr->Binding=strdup(pBinding+1);
		fPtr->nOverride = 1;
	}
	else {
		fPtr->Binding=strdup(pBinding);
		fPtr->nOverride = 0;
	}

	// can't allocate memory
	if (fPtr->Binding == NULL) {
		AppendText(EverythingUI, DIR_SYSTEM, "Error: Can't allocate memory for fPtr->Binding for %s.\n", pBinding);
		return (NULL);
	}

	// populate the property field
	fPtr->Property=strdup(pProperty);

	// can't allocate memory
	if (fPtr->Property == NULL) {
		AppendText(EverythingUI, DIR_SYSTEM, "Error: Can't allocate memory for fPtr->Property for %s.\n", pProperty);
		return (NULL);
	}

	// store the depth
	fPtr->nDepth = nDepth;

	AppendText(EverythingUI, DIR_TRACE, "%*s Adding -> %s %s %s [%d]", fPtr->nDepth+1, "", fPtr->Object, fPtr->Binding, fPtr->Property, fPtr->nDepth);

	// nothing comes after this link
	fPtr->Next=NULL;

	// return from function
	return (fPtr);

}	// end of AddFactToList

void DoListWalk(GUI_Elements *EverythingUI)
{
	struct Flink *fPtr;

	// From the start of the linked list (root), display all facts
	for (fPtr=Root; fPtr != NULL; fPtr=fPtr->Next) {

		if (fPtr->nOverride > IGNORE_FACT) {

			AppendText(EverythingUI, DIR_OUT, "%*s%s %s %s [%d]", fPtr->nDepth+1, "", fPtr->Object, fPtr->Binding, fPtr->Property, fPtr->nDepth);
		}
		else
				AppendText(EverythingUI, DIR_TRACE, "** Ignored -> %*s%s %s %s [%d]", fPtr->nDepth+1, "", fPtr->Object, fPtr->Binding, fPtr->Property, fPtr->nDepth);
	}

}	// end of DoListWalk

void DeleteAllFacts(void)
{
	struct Flink *fPtr = NULL, *nPtr = NULL;

	// From the start of the linked list (root), delete all facts
	for (fPtr=Root; fPtr != NULL; fPtr=nPtr) {

		// get the pointer to the next fact before we delete this one
		nPtr = fPtr->Next;

		// free up the memory for the strdup'd strings
		free(fPtr->Object);
		free(fPtr->Binding);
		free(fPtr->Property);

		// null the pointer - safe but not strictly required
		fPtr->Next = NULL;

		// free the memory for the structure itself
		free(fPtr);
	}

	// set start and end of the linked list to null
	// to indicate that the linked list is empty
	Root = End = NULL;

}	// end of DeleteAllFacts

void NormalizeFacts(int nScope, GUI_Elements *EverythingUI)
{
	struct Flink *fPtr;

	if (nScope == ALL_OBJECTS) {

		// ALL_OBJECTS indicates that we should do this for all facts in the list
		for (fPtr=Root; fPtr != NULL; fPtr=fPtr->Next) {

			if (fPtr->nOverride == 1) {

				AppendText(EverythingUI, DIR_TRACE, "** Overrider -> %s %s %s [%d]", fPtr->Object, fPtr->Binding, fPtr->Property, fPtr->nDepth);
				OverrideInheritedFacts(nScope, EverythingUI, fPtr);
			}
		}
	}
	else if (nScope == OBJECT_TWO) {

		// OBJECT_TWO indicates that we should do this for the facts in the list that relate to object 2.
		// These are located between the Breakpoint dummy fact and the End of the list
		for (fPtr=Breakpoint->Next; fPtr != NULL; fPtr=fPtr->Next) {

			if (fPtr->nOverride == 1) {

				AppendText(EverythingUI, DIR_TRACE, "** Overrider -> %s %s %s [%d]", fPtr->Object, fPtr->Binding, fPtr->Property, fPtr->nDepth);
				OverrideInheritedFacts(nScope, EverythingUI, fPtr);
			}
		}
	}

}	// end of NormalizeFacts

void OverrideInheritedFacts(int nScope, GUI_Elements *EverythingUI, struct Flink *fPtr)
{
	struct Flink *kPtr;

	if (nScope == ALL_OBJECTS) {

		// ALL_OBJECTS indicates that we should do this for all facts in the list
		for (kPtr=Root; kPtr != NULL; kPtr=kPtr->Next) {

			// only bother checking if this fact is inherited (ie. deeper nDepth)
			if (kPtr->nDepth > fPtr->nDepth) {

				// if these facts have the same binding, flag the inherited one as ignored
				if (strcasecmp(kPtr->Binding, fPtr->Binding) == 0) {

					kPtr->nOverride = IGNORE_FACT;
				}
			}
		}
	}
	else if (nScope == OBJECT_TWO) {

		// OBJECT_TWO indicates that we should do this for the facts in the list that relate to object 2.
		// These are located between the Breakpoint dummy fact and the End of the list
		for (kPtr=Breakpoint->Next; kPtr != NULL; kPtr=kPtr->Next) {

			// only bother checking if this fact is inherited (ie. deeper nDepth)
			if (kPtr->nDepth > fPtr->nDepth) {

				// if these facts have the same binding, flag the inherited one as ignored
				if (strcasecmp(kPtr->Binding, fPtr->Binding) == 0) {

					kPtr->nOverride = IGNORE_FACT;
				}
			}
		}
	}

}	// end of OverrideInheritedFacts

int CheckForNeedlessRecursion(GUI_Elements *EverythingUI, char *pProperty, int nDepth)
{
	struct Flink *fPtr;
	int nFound = USABLE_FACT;

	// walk the list of gathered facts
	for (fPtr=Root; fPtr != NULL; fPtr=fPtr->Next) {

		// if the current fact has an object the same as the property passed for checking
		// and the object is at the same or a lower depth, flag back that we should ignore
		// this property and not do any deeper searchings
		if ((strcasecmp(fPtr->Object, pProperty) == 0) && (fPtr->nDepth <= nDepth)) {

			// send back a flag meaning ignore this fact
			nFound = IGNORE_FACT;
			// capture this in the trace window
			AppendText(EverythingUI, DIR_TRACE, "** Ignored to avoid recursion -> %s [%d]", pProperty, nDepth);
			// don't bother walking the rest of the list
			break;
		}
	}

	// send back the response
	return (nFound);

}	// end of void CheckForNeedlessRecursion

void ParseUserInput(char *QueryString, GUI_Elements *EverythingUI)
{
	char *cPtr, *firstPtr, *secondPtr, szSeps[]=" \n";

	int i, nCommand = (-1), sDepth = gDepth;

	// for the command history functions
	char *strHistory;

	// if it is not an empty string record it
	if (strlen(QueryString) > 0)
			strHistory = strdup(QueryString);
	else
			return;

	// get the first word of the user input
	cPtr = strtok(QueryString, szSeps);

	if (cPtr == NULL)
			return;

	// see if it is in the list of known commands
	for (i=0; dCommands[i].Command != NULL; i++) {

		// check abbreviations first
		if (strcmp(dCommands[i].Abbrev, cPtr) == 0) {
			// abbreviated command found
			nCommand = dCommands[i].nCommand;
			break;
		}
		// check the full name version if the abbrev didn't match
		else if (strcmp(dCommands[i].Command, cPtr) == 0) {
			// full command found
			nCommand = dCommands[i].nCommand;
			break;
		}
	}

	// add command to the history
	if ((nCommand != COMMAND_REDO) && (nCommand != COMMAND_HISTORY))
			AddCommandToList(strHistory);

	// look for a first parameter
	firstPtr = strtok(NULL, szSeps);

	// possibly look for a second
	if (firstPtr != NULL)
			secondPtr = strtok(NULL, szSeps);

	// no command given, treat as an object search
	if (nCommand < 0) {
		nCommand = COMMAND_OBJECT;
		secondPtr = firstPtr;
		firstPtr = cPtr;
	}

	// perform the appropriate command action
	switch (nCommand) {

		// turn on the system trace functions
		case COMMAND_TRON:
			// AppendText(EverythingUI, DIR_SYSTEM, "Process trace ON");
			gtk_widget_show(EverythingUI->TraceWnd);
			break;

		// turn off the system trace functions
		case COMMAND_TROFF:
			// AppendText(EverythingUI, DIR_SYSTEM, "Process trace OFF");
			gtk_widget_hide(EverythingUI->TraceWnd);
			break;

		// perform the object-centric search function
		case COMMAND_OBJECT:
			// if nothing was specified to search for, list objects
			if (firstPtr == NULL) {
				ListAllObjects(OBJECT_FIELD, EverythingUI);
				break;
			}

			// was a depth specified?
			if (secondPtr != NULL)
					sDepth = (isdigit(*secondPtr)) ? atoi(secondPtr) : gDepth;

			// perform the search
			PerformObjectQuery(firstPtr, sDepth, EverythingUI);
			break;

		// perform the query function
		case COMMAND_QUERY:
			// if nothing was specified to search for, list objects
			if (firstPtr == NULL) {
				ListAllObjects(OBJECT_FIELD, EverythingUI);
				break;
			}

			// was a property specified - if not do an object query
			if (secondPtr == NULL) {

				PerformObjectQuery(firstPtr, sDepth, EverythingUI);
			}
			else
					// perform the link query search
					PerformLinkQuery(firstPtr, secondPtr, DEEP_SEARCH, EverythingUI);
			break;

		// perform the conflux function
		case COMMAND_CONFLUX:
			// both parameters must be specified to conflux, otherwise do nothing
			if ((firstPtr == NULL) || (secondPtr == NULL)) {
				AppendText(EverythingUI, DIR_OUT, "Usage: %s", dCommands[COMMAND_CONFLUX].Usage);
				break;
			}
			else
					// perform the conflux query search
					PerformConfluxQuery(firstPtr, secondPtr, EverythingUI);
			break;

		// perform the property-centric search function
		case COMMAND_PROPERTY:
			// if nothing was specified to search for, list properties
			if (firstPtr == NULL) {
				// list all properties
				ListAllObjects(PROPERTY_FIELD, EverythingUI);
				break;
			}

			// was a depth specified?
			if (secondPtr != NULL)
					sDepth = (isdigit(*secondPtr)) ? atoi(secondPtr) : gDepth;

			// perform the search
			PerformPropertyQuery(firstPtr, sDepth, EverythingUI);
			break;

		// perform the binding-centric search function
		case COMMAND_BINDING:
			// if nothing was specified to search for, list bindings
			if (firstPtr == NULL) {
				// list all bindings
				ListAllObjects(BINDING_FIELD, EverythingUI);
				break;
			}

			// perform the search
			PerformBindingQuery(firstPtr, EverythingUI);
			break;

		// perform the depth-setting function
		case COMMAND_DEPTH:
			// was a depth specified?
			if (firstPtr != NULL)
					gDepth = (isdigit(*firstPtr)) ? atoi(firstPtr) : gDepth;

			AppendText(EverythingUI, DIR_OUT, "Default search depth is %d", gDepth);
			break;

		// perform the fact listing function
		case COMMAND_FACTS:
			ListFacts(EverythingUI, NATIVE_ORDER);
			break;

		// perform the first order fact listing function
		case COMMAND_FOL:
			ListFacts(EverythingUI, FIRST_ORDER);
			break;

		// save the curent session as a text file
		case COMMAND_SAVE:
			WriteOutFile(EverythingUI);
			break;

		// perform the about (help) function
		case COMMAND_ABOUT:
			PerformAboutCommand(EverythingUI);
			break;

		// give a listing of the dot commands
		case COMMAND_HELP:
			AppendText(EverythingUI, DIR_SYSTEM, "EIS %s :: Compiled on %s at %s.", EIS_VERSION, __DATE__, __TIME__);

			for (i=0; dCommands[i].Command != NULL; i++)
					AppendText(EverythingUI, DIR_SYSTEM, "%-28s - %s", dCommands[i].Help1, dCommands[i].Help2);

			AppendText(EverythingUI, DIR_SYSTEM, "[Default search depth is %d]", gDepth);
			BLANK_LINE;
			break;

		// exit from the program - perform the exit function
		case COMMAND_EXIT:
			gtk_widget_destroy(EverythingUI->MainWnd);
			break;

		// perform the fact counting function
		case COMMAND_COUNT:
			CountFacts(EverythingUI);
			break;

		// perform the factcommand history function
		case COMMAND_HISTORY:
			PerformHistoryWalk(EverythingUI);
			break;

		// perform the factcommand history function
		case COMMAND_REDO:
			if (firstPtr == NULL)
					AppendText(EverythingUI, DIR_SYSTEM, "%s: No command number supplied", dCommands[ nCommand ].Abbrev);
			else {
				PerformRedoCommand(EverythingUI, atoi(firstPtr));
			}
			break;

		default: break;
	}

	// for legibility
	BLANK_LINE;
	BLANK_TRACE;

}	// end of ParseUserInput

void AppendText(GUI_Elements *EverythingUI, int nDirection, char *NewString, ...)
{
	GtkTextBuffer *bufferText = NULL;
	GtkTextBuffer *bufferTrace = NULL;
	GtkTextIter iter;
	char text[250];
	char cPrefix;

	GtkTextMark *gTextMark = gtk_text_mark_new(NULL, FALSE);

	const gchar *tag_name;

	char TextBuffer[225];
	va_list argptr;

	// if required compose the multiple arguments into a single line of text
	if (NewString != NULL) {
		va_start(argptr, NewString);
		vsprintf(TextBuffer, NewString, argptr);
		va_end(argptr);
	}

	// determine the line prefix character and font attributes
	// according to the type of message to display
	switch (nDirection) {

		// user entry
		case DIR_IN: cPrefix = '<';
				// tag_name = "bold";
				tag_name = NULL;
				break;

		// EIS output
		case DIR_OUT: cPrefix = '>';
				// tag_name = NULL;
				tag_name = "bold";
				break;

		// system message
		case DIR_SYSTEM: cPrefix = '+';
				tag_name = "italic";
				break;

		// for a blank line
		case DIR_BLANK: cPrefix = ' ';
				tag_name = NULL;
				break;

		case DIR_TRACE: cPrefix = ':';
				// tag_name = "italic";
				tag_name = NULL;
				break;

		// if any other message type is requested show a '!'
		default: cPrefix = '!';
				tag_name = NULL;
				break;
	}

	// prepend the line type character
	sprintf(text, "%c %s\n", cPrefix, TextBuffer);

	// get a handle on the buffer from the appropriate window
	if (nDirection != DIR_TRACE) {

		// for everything but trace messages, use the main window
		bufferText = gtk_text_view_get_buffer(GTK_TEXT_VIEW (EverythingUI->TextView));

		// get the location of the last character + 1
		gtk_text_buffer_get_end_iter(bufferText, &iter);

		// add a mark to the buffer, and place it at the end of the buffer
		gtk_text_buffer_add_mark(bufferText, gTextMark, &iter);

		// display the line of text by adding it to the buffer
		gtk_text_buffer_insert_with_tags_by_name(bufferText, &iter, text, -1, tag_name, NULL);

		// scroll the window to the new mark (the end of the buffer)
		gtk_text_view_scroll_to_mark(GTK_TEXT_VIEW (EverythingUI->TextView), gTextMark, 0.0, FALSE, 0.0, 0.0);

		// delete the mark again
		gtk_text_buffer_delete_mark(bufferText, gTextMark);
	}
	else {

		// for trace messages, use the system trace window
		bufferTrace = gtk_text_view_get_buffer(GTK_TEXT_VIEW (EverythingUI->TraceView));

		// get the location of the last character + 1
		gtk_text_buffer_get_end_iter(bufferTrace, &iter);

		// add a mark to the buffer, and place it at the end of the buffer
		gtk_text_buffer_add_mark(bufferTrace, gTextMark, &iter);

		// display the line of text by adding it to the buffer
		gtk_text_buffer_insert_with_tags_by_name(bufferTrace, &iter, text, -1, tag_name, NULL);

		// scroll the window to the new mark (the end of the buffer)
		gtk_text_view_scroll_to_mark(GTK_TEXT_VIEW (EverythingUI->TraceView), gTextMark, 0.0, FALSE, 0.0, 0.0);

		// delete the mark again
		gtk_text_buffer_delete_mark(bufferTrace, gTextMark);
	}

}	// end of AppendText

void ListAllObjects(int nField, GUI_Elements *EverythingUI)
{
	char *pSearchClue[] = {
		"object",
		"binding",
		"property",
		NULL
	};

	char szTemplate[]="SELECT DISTINCT %s FROM facts ORDER BY %s";
	char szSQLString[200]="";
	char *pDBField;
	int nResponse;
	sqlite3_stmt *stmt = NULL;

	// in case it isn't showing, display the system trace window
	gtk_widget_show(EverythingUI->TraceWnd);

	// create the SQL string
	sprintf(szSQLString, szTemplate, pSearchClue[nField], pSearchClue[nField]);

	// AppendText(EverythingUI, DIR_TRACE, "%s", szSQLString);

	// prepare the SQL statement
	nResponse = sqlite3_prepare_v2(knowledgeDB, szSQLString, -1, &stmt, NULL);

	// if not OK, bomb out
	if (nResponse != SQLITE_OK)
			return;

	// if OK, start to walk through the record set
	nResponse = sqlite3_step(stmt);

	while (nResponse == SQLITE_ROW) {

		// take a copy of the required field - will always be field 0
		// because there is only a single column in the record set
		pDBField = (char*) sqlite3_column_text(stmt, 0);

		if (*pDBField != '<')
				AppendText(EverythingUI, DIR_TRACE, " %s", pDBField);

		// check for the next record
		nResponse = sqlite3_step(stmt);
	}

	// close off the sql statement
	sqlite3_finalize(stmt);

}	// end of ListAllObjects

void ListFacts(GUI_Elements *EverythingUI, int nFact_Order)
{
	char szSQLString[]="SELECT * FROM facts";
	char *pObject, *pBinding, *pProperty;
	int nResponse;
	sqlite3_stmt *stmt = NULL;

	// in case it isn't showing, display the system trace window
	gtk_widget_show(EverythingUI->TraceWnd);

	// AppendText(EverythingUI, DIR_TRACE, "%s", szSQLString);

	// prepare the SQL statement
	nResponse = sqlite3_prepare_v2(knowledgeDB, szSQLString, -1, &stmt, NULL);

	// if not OK, bomb out
	if (nResponse != SQLITE_OK)
			return;

	// if OK, start to walk through the record set
	nResponse = sqlite3_step(stmt);

	while (nResponse == SQLITE_ROW) {

		// take a copy of the three elements of the fact
		pObject = (char*) sqlite3_column_text(stmt, OBJECT_FIELD);
		pBinding = (char*) sqlite3_column_text(stmt, BINDING_FIELD);
		pProperty = (char*) sqlite3_column_text(stmt, PROPERTY_FIELD);

		if (nFact_Order == NATIVE_ORDER) {
			AppendText(EverythingUI, DIR_TRACE, " %s -> %s -> %s", pObject, (*pBinding != '<') ? pBinding : pBinding+1, pProperty);
		}
		else
				AppendText(EverythingUI, DIR_TRACE, " %s(%s, %s)", (*pBinding != '<') ? pBinding : pBinding+1, pObject, pProperty);
				// AppendText(EverythingUI, DIR_TRACE, "%s(%s).", pProperty, pObject);

		// check for the next record
		nResponse = sqlite3_step(stmt);
	}

	// close off the sql statement
	sqlite3_finalize(stmt);

}	// end of ListFacts

void CountFacts(GUI_Elements *EverythingUI)
{
	char szSQLString[]="SELECT count(*) FROM facts";
	int nCount, nResponse;
	sqlite3_stmt *stmt = NULL;

	// in case it isn't showing, display the system trace window
	gtk_widget_show(EverythingUI->TraceWnd);

	// AppendText(EverythingUI, DIR_TRACE, "%s", szSQLString);

	// prepare the SQL statement
	nResponse = sqlite3_prepare_v2(knowledgeDB, szSQLString, -1, &stmt, NULL);

	// if not OK, bomb out
	if (nResponse != SQLITE_OK)
			return;

	// if OK, start to walk through the record set
	nResponse = sqlite3_step(stmt);

	if (nResponse != SQLITE_ROW)
			return;

	// extract the count
	nCount=sqlite3_column_int(stmt, COUNT_FIELD);

	// close off the sql statement
	sqlite3_finalize(stmt);

	// show the result
	AppendText(EverythingUI, DIR_TRACE, "%d facts in knowledge base", nCount);

}	// end of CountFacts

void WriteOutFile(GUI_Elements *EverythingUI)
{
	GError					*err = NULL;
	gchar						*text;
	gboolean				result;
	GtkTextBuffer		*buffer;
	GtkTextIter			start, end;
	char						szFilename[35] = "";

	time_t t;
	struct tm *tmNow;

	// make sure the gui is finished with any queued instructions before going further
	while (gtk_events_pending())
			gtk_main_iteration();

	// get contents of buffer
	buffer = gtk_text_view_get_buffer(GTK_TEXT_VIEW (EverythingUI->TextView));

	gtk_text_buffer_get_start_iter(buffer, &start);
	gtk_text_buffer_get_end_iter(buffer, &end);
	text = gtk_text_buffer_get_text(buffer, &start, &end, FALSE);
	gtk_text_buffer_set_modified(buffer, FALSE);

	t = time(NULL);
	tmNow = localtime(&t);

	if (tmNow == NULL) {

		// create a filename from the compile stamp
		sprintf(szFilename, "EIS_%s_%s.txt", __DATE__, __TIME__);
	}
	else {

		// create a filename from the time
		strftime(szFilename, 35, "EIS_%F_%H-%M-%S.txt", tmNow);
	}

	result = g_file_set_contents (szFilename, text, -1, &err);

	if (result == FALSE) {

		// error saving file, show message to user
		AppendText(EverythingUI, DIR_SYSTEM, "Couldn't save file: %s", szFilename);
		AppendText(EverythingUI, DIR_SYSTEM, (err->message));
	}
	else
			AppendText(EverythingUI, DIR_SYSTEM, "Saved file: %s", szFilename);

	// free the text memory
	g_free (text);

}	// end of WriteOutFile

void PerformAboutCommand(GUI_Elements *EverythingUI)
{
	char szSoftwareVersion[100];

	const char *Authors[] = {
		"Dave McKay <dave@dpocompliance.co.uk>",
		NULL
	};

	const char *Catering[] = {
		"Pat MacKenzie <pcamackenzie@btconnect.com>",
		NULL
	};

	const char *Libraries[] = {
		"GCC https://gcc.gnu.org/",
		"GTK+ http://www.gtk.org/",
		"Glade https://glade.gnome.org/",
		"SQLite3 https://www.sqlite.org/",
		NULL
	};

	sprintf(szSoftwareVersion, "EIS %s :: Compiled on %s at %s.", EIS_VERSION, __DATE__, __TIME__);

	// set the icon
	GdkPixbuf *pixbuf = gdk_pixbuf_new_from_file("eis_icon.png", NULL);

	// create the dialog box
	GtkWidget *AboutDialog = gtk_about_dialog_new();

	// set the data elements of the About box
	gtk_about_dialog_set_program_name(GTK_ABOUT_DIALOG(AboutDialog), "EIS");
	gtk_about_dialog_set_version(GTK_ABOUT_DIALOG(AboutDialog), szSoftwareVersion);
	gtk_about_dialog_set_copyright(GTK_ABOUT_DIALOG(AboutDialog), "\u00A9 Dave McKay 2013-23");
	gtk_about_dialog_set_comments(GTK_ABOUT_DIALOG(AboutDialog), "Knowledge representation, retrieval and interrogation.");
	gtk_about_dialog_set_website(GTK_ABOUT_DIALOG(AboutDialog), "http://www.dpocompliance.co.uk//");
	gtk_about_dialog_set_website_label(GTK_ABOUT_DIALOG(AboutDialog), "McKay Consulting");
	gtk_about_dialog_set_authors(GTK_ABOUT_DIALOG(AboutDialog), Authors);
	gtk_about_dialog_add_credit_section(GTK_ABOUT_DIALOG(AboutDialog), "Cups of Tea", Catering);
	gtk_about_dialog_add_credit_section(GTK_ABOUT_DIALOG(AboutDialog), "Libraries / Tools / APIs", Libraries);
	gtk_about_dialog_set_logo(GTK_ABOUT_DIALOG(AboutDialog), pixbuf);

	// don't need the handle to the icon anymore
	g_object_unref(pixbuf);
	pixbuf = NULL;

	// make the About dialog transient to the main window and a modal dialog
	gtk_window_set_modal((GtkWindow *) AboutDialog, TRUE);
	gtk_window_set_transient_for ((GtkWindow *) AboutDialog, (GtkWindow *) EverythingUI->MainWnd);

	// display the about box
	gtk_dialog_run(GTK_DIALOG(AboutDialog));

	// don't need the dialog box anymore
	gtk_widget_destroy(AboutDialog);

}	// end of PerformAboutCommand

struct Clink *AddCommandToList(char *pUserInput)
{
	struct Clink *fPtr;
	int nCommandID = 1;	// this will be changed if it isn't the first command

	// no commands in the list yet
	if (cRoot == NULL) {

		// get memory for the new command structure
		fPtr = (struct Clink *) malloc(sizeof(struct Clink));

		// can't allocate memory
		if (fPtr == NULL) {

			return (NULL);
		}

		// set this command as the start of the linked list
		cRoot = cEnd = fPtr;
	}
	// commands exist, append new command to the list
	else {
		// get memory for the new command structure
		fPtr = (struct Clink *) malloc(sizeof(struct Clink));

		// can't allocate memory
		if (fPtr == NULL) {

			return (NULL);
		}

		// work out our command ID
		nCommandID = cEnd->nCommandID + 1;
		// point the hitherto last command to the new last command
		cEnd->Next=fPtr;

		// this is now the end of the linked list
		cEnd=fPtr;
	}

	// populate the user input field
	fPtr->UserInput = strdup(pUserInput);

	// can't allocate memory
	if (fPtr->UserInput == NULL) {

		return (NULL);
	}

	// create the command ID
	fPtr->nCommandID = nCommandID;

	// nothing comes after this link
	fPtr->Next=NULL;

	// return from function
	return (fPtr);

}	// end of AddCommandToList

void PerformHistoryWalk(GUI_Elements *EverythingUI)
{
	struct Clink *fPtr;

	// From the start of the linked list (root), display all commands
	for (fPtr=cRoot; fPtr != NULL; fPtr=fPtr->Next) {

		// display the command number (list position) and command string
		AppendText(EverythingUI, DIR_SYSTEM, "%3d %s", fPtr->nCommandID, fPtr->UserInput);
	}

}	// end of PerformHistoryWalk

void PerformRedoCommand(GUI_Elements *EverythingUI, int nCommand)
{
	struct Clink *fPtr;
	int nFound = 0;

	char *strCommand;

	// From the start of the linked list (root), look for the requested Command
	for (fPtr=cRoot; fPtr != NULL; fPtr=fPtr->Next) {

		// if we've found the command
		if (fPtr->nCommandID == nCommand) {

			// flag that we've found it send it back into the parser as a new command
			nFound++;
			strCommand = strdup (fPtr->UserInput);

			if (strCommand != NULL) {

				// send it to be actioned by the parser
				ParseUserInput(strCommand, EverythingUI);
				// release the memory again
				free(strCommand);
			}
			break;
		}
	}

	// if it wasn't found let them know
	if (nFound == 0)
			AppendText(EverythingUI, DIR_SYSTEM, "redo: Command %d not found\n++++", nCommand);

}	// end of PerformRedoCommand
