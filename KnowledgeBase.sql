PRAGMA foreign_keys=OFF;
BEGIN TRANSACTION;

CREATE TABLE "facts" (
	"object" TEXT,
	"binding" TEXT,
	"property" TEXT
);

INSERT INTO "facts" VALUES('animate','is','living');
INSERT INTO "facts" VALUES('inanimate','is','nonliving');
INSERT INTO "facts" VALUES('living','is','mortal');
INSERT INTO "facts" VALUES('mortal','is','will_die');
INSERT INTO "facts" VALUES('vertebrate','has','backbone');
INSERT INTO "facts" VALUES('invertebrate','has','no_backbone');
INSERT INTO "facts" VALUES('animal','is','animate');
INSERT INTO "facts" VALUES('vertebrate','is','animal');
INSERT INTO "facts" VALUES('invertebrate','is','animal');
INSERT INTO "facts" VALUES('endothermic','is','warm_blooded');
INSERT INTO "facts" VALUES('ectothermic','is','cold_blooded');
INSERT INTO "facts" VALUES('mammal','is','vertebrate');
INSERT INTO "facts" VALUES('mammal','is','endothermic');
INSERT INTO "facts" VALUES('mammal','gives_birth','live_young');
INSERT INTO "facts" VALUES('mammal','body_covering','hair_fur');
INSERT INTO "facts" VALUES('mankind','is','mammal');
INSERT INTO "facts" VALUES('human','is','mankind');
INSERT INTO "facts" VALUES('human','is','biped');
INSERT INTO "facts" VALUES('human','can','walk');
INSERT INTO "facts" VALUES('human','can','run');
INSERT INTO "facts" VALUES('human','can','climb');
INSERT INTO "facts" VALUES('human','can','swim');
INSERT INTO "facts" VALUES('human','can','crawl');
INSERT INTO "facts" VALUES('man','is','human');
INSERT INTO "facts" VALUES('woman','is','human');
INSERT INTO "facts" VALUES('man','is','male');
INSERT INTO "facts" VALUES('woman','is','female');
INSERT INTO "facts" VALUES('boy','is','human');
INSERT INTO "facts" VALUES('girl','is','human');
INSERT INTO "facts" VALUES('boy','is','male');
INSERT INTO "facts" VALUES('girl','is','female');
INSERT INTO "facts" VALUES('whale','is','mammal');
INSERT INTO "facts" VALUES('whale','can','swim');
INSERT INTO "facts" VALUES('whale','<body_covering','bald_smooth');
INSERT INTO "facts" VALUES('quadruped','has','four_legs');
INSERT INTO "facts" VALUES('biped','has','two_legs');
INSERT INTO "facts" VALUES('venomous','uses','venom');
INSERT INTO "facts" VALUES('venom','is','toxin');
INSERT INTO "facts" VALUES('toxin','is','poison');
INSERT INTO "facts" VALUES('monotreme','is','mammal');
INSERT INTO "facts" VALUES('monotreme','<gives_birth','lays_eggs');
INSERT INTO "facts" VALUES('monotreme','has','coacla');
INSERT INTO "facts" VALUES('platypus','is','monotreme');
INSERT INTO "facts" VALUES('platypus','has','beak');
INSERT INTO "facts" VALUES('echidna','is','monotreme');
INSERT INTO "facts" VALUES('spiny_anteater','is','echidna');
INSERT INTO "facts" VALUES('spiny_anteater','is','quadruped');
INSERT INTO "facts" VALUES('ape','is','mammal');
INSERT INTO "facts" VALUES('ape','is','biped');
INSERT INTO "facts" VALUES('ape','can','walk');
INSERT INTO "facts" VALUES('ape','can','run');
INSERT INTO "facts" VALUES('ape','can','climb');
INSERT INTO "facts" VALUES('ape','can','swim');
INSERT INTO "facts" VALUES('ape','can','crawl');
INSERT INTO "facts" VALUES('orangutan','is','ape');
INSERT INTO "facts" VALUES('gibbon','is','ape');
INSERT INTO "facts" VALUES('bonobo','is','ape');
INSERT INTO "facts" VALUES('chimpanzee','is','ape');
INSERT INTO "facts" VALUES('cheetah','is','mammal');
INSERT INTO "facts" VALUES('cheetah','is','quadruped');
INSERT INTO "facts" VALUES('cheetah','can','run');
INSERT INTO "facts" VALUES('horse','is','mammal');
INSERT INTO "facts" VALUES('horse','is','quadruped');
INSERT INTO "facts" VALUES('horse','can','run');
INSERT INTO "facts" VALUES('bird','is','vertebrate');
INSERT INTO "facts" VALUES('bird','is','biped');
INSERT INTO "facts" VALUES('bird','is','endothermic');
INSERT INTO "facts" VALUES('bird','has','beak');
INSERT INTO "facts" VALUES('bird','has','wings');
INSERT INTO "facts" VALUES('bird','has','coacla');
INSERT INTO "facts" VALUES('bird','body_covering','feathers');
INSERT INTO "facts" VALUES('bird','gives_birth','lays_eggs');
INSERT INTO "facts" VALUES('flighted_bird','is','bird');
INSERT INTO "facts" VALUES('flighted_bird','can','fly');
INSERT INTO "facts" VALUES('unflighted_bird','is','bird');
INSERT INTO "facts" VALUES('pigeon','is','flighted_bird');
INSERT INTO "facts" VALUES('sparrow','is','flighted_bird');
INSERT INTO "facts" VALUES('raven','is','flighted_bird');
INSERT INTO "facts" VALUES('thrush','is','flighted_bird');
INSERT INTO "facts" VALUES('swallow','is','flighted_bird');
INSERT INTO "facts" VALUES('swift','is','flighted_bird');
INSERT INTO "facts" VALUES('penguin','is','unflighted_bird');
INSERT INTO "facts" VALUES('penguin','can','swim');
INSERT INTO "facts" VALUES('ostrich','is','unflighted_bird');
INSERT INTO "facts" VALUES('ostrich','can','run');
INSERT INTO "facts" VALUES('fish','is','ectothermic');
INSERT INTO "facts" VALUES('fish','can','swim');
INSERT INTO "facts" VALUES('flying_fish','is','fish');
INSERT INTO "facts" VALUES('flying_fish','can','fly');
INSERT INTO "facts" VALUES('shark','is','fish');
INSERT INTO "facts" VALUES('fish','body_covering','scales');
INSERT INTO "facts" VALUES('reptile','is','vertebrate');
INSERT INTO "facts" VALUES('reptile','is','ectothermic');
INSERT INTO "facts" VALUES('reptile','body_covering','scales');
INSERT INTO "facts" VALUES('reptile','gives_birth','lays_eggs');
INSERT INTO "facts" VALUES('arthropod','is','invertebrate');
INSERT INTO "facts" VALUES('arthropod','has','exoskeleton');
INSERT INTO "facts" VALUES('arthropod','has','jointed_body');
INSERT INTO "facts" VALUES('insect','is','arthropod');
INSERT INTO "facts" VALUES('metameric','has','linear_segments');
INSERT INTO "facts" VALUES('flighted_insect','is','insect');
INSERT INTO "facts" VALUES('flighted_insect','can','fly');
INSERT INTO "facts" VALUES('flighted_insect','has','wings');
INSERT INTO "facts" VALUES('spider','is','arthropod');
INSERT INTO "facts" VALUES('spider','can','walk');
INSERT INTO "facts" VALUES('spider','eats','fly');
INSERT INTO "facts" VALUES('spider','spins','web');
INSERT INTO "facts" VALUES('centipede','is','arthropod');
INSERT INTO "facts" VALUES('centipede','can','walk');
INSERT INTO "facts" VALUES('centipede','is','metameric');
INSERT INTO "facts" VALUES('bee','is','flighted_insect');
INSERT INTO "facts" VALUES('bee','is','venomous');
INSERT INTO "facts" VALUES('wasp','is','flighted_insect');
INSERT INTO "facts" VALUES('wasp','is','venomous');
INSERT INTO "facts" VALUES('bluebottle','is','flighted_insect');
INSERT INTO "facts" VALUES('ant','is','insect');
INSERT INTO "facts" VALUES('cnidaria','is','invertebrate');
INSERT INTO "facts" VALUES('jellyfish','is','cnidaria');
INSERT INTO "facts" VALUES('jellyfish','is','legless');
INSERT INTO "facts" VALUES('jellyfish','can','swim');
INSERT INTO "facts" VALUES('box_jellyfish','is','jellyfish');
INSERT INTO "facts" VALUES('box_jellyfish','is','venomous');
INSERT INTO "facts" VALUES('snake','is','reptile');
INSERT INTO "facts" VALUES('snake','is','legless');
INSERT INTO "facts" VALUES('lizard','is','reptile');
INSERT INTO "facts" VALUES('lizard','is','quadruped');
INSERT INTO "facts" VALUES('cobra','is','snake');
INSERT INTO "facts" VALUES('cobra','is','venomous');
INSERT INTO "facts" VALUES('anaconda','is','snake');
INSERT INTO "facts" VALUES('anaconda','can','swim');
INSERT INTO "facts" VALUES('crocodile','is','reptile');
INSERT INTO "facts" VALUES('crocodile','is','quadruped');
INSERT INTO "facts" VALUES('crocodile','can','walk');
INSERT INTO "facts" VALUES('crocodile','can','run');
INSERT INTO "facts" VALUES('crocodile','can','swim');
INSERT INTO "facts" VALUES('rodent','is','mammal');
INSERT INTO "facts" VALUES('rodent','is','quadruped');
INSERT INTO "facts" VALUES('rat','is','rodent');
INSERT INTO "facts" VALUES('mouse','is','rodent');
INSERT INTO "facts" VALUES('hamster','is','rodent');
INSERT INTO "facts" VALUES('shrew','is','rodent');
INSERT INTO "facts" VALUES('slug','is','ectothermic');
INSERT INTO "facts" VALUES('slug','is','invertebrate');
INSERT INTO "facts" VALUES('slug','gives_birth','lays_eggs');
INSERT INTO "facts" VALUES('mineral','is','inanimate');
INSERT INTO "facts" VALUES('metal','is','inanimate');
INSERT INTO "facts" VALUES('rock','is','mineral');
INSERT INTO "facts" VALUES('stone','is','mineral');
INSERT INTO "facts" VALUES('flint','is','stone');
INSERT INTO "facts" VALUES('granite','is','stone');
INSERT INTO "facts" VALUES('limestone','is','stone');
INSERT INTO "facts" VALUES('sandstone','is','stone');
INSERT INTO "facts" VALUES('alloy','is','metal');
INSERT INTO "facts" VALUES('bronze','is','alloy');
INSERT INTO "facts" VALUES('copper','is','metal');
INSERT INTO "facts" VALUES('tin','is','alloy');
INSERT INTO "facts" VALUES('iron','is','metal');
INSERT INTO "facts" VALUES('machine','is','manufactured');
INSERT INTO "facts" VALUES('machine','has','function');
INSERT INTO "facts" VALUES('machine','is','inanimate');
INSERT INTO "facts" VALUES('weaver','is','human');
INSERT INTO "facts" VALUES('weaver','spins','cloth');
INSERT INTO "facts" VALUES('croupier','is','human');
INSERT INTO "facts" VALUES('croupier','spins','roulette_wheel');
INSERT INTO "facts" VALUES('canine','is','mammal');
INSERT INTO "facts" VALUES('canine','is','quadruped');
INSERT INTO "facts" VALUES('feline','is','mammal');
INSERT INTO "facts" VALUES('feline','is','quadruped');
INSERT INTO "facts" VALUES('dog','is','canine');
INSERT INTO "facts" VALUES('cat','is','feline');
INSERT INTO "facts" VALUES('domesticated_dog','is','dog');
INSERT INTO "facts" VALUES('domesticated_cat','is','cat');
INSERT INTO "facts" VALUES('wild_dog','is','dog');
INSERT INTO "facts" VALUES('wild_cat','is','cat');
INSERT INTO "facts" VALUES('lion','is','wild_cat');
INSERT INTO "facts" VALUES('dingo','is','wild_dog');
INSERT INTO "facts" VALUES('labrador','is','domesticated_dog');
INSERT INTO "facts" VALUES('poodle','is','domesticated_dog');
INSERT INTO "facts" VALUES('tabby','is','domesticated_cat');
INSERT INTO "facts" VALUES('badger','is','mammal');
INSERT INTO "facts" VALUES('badger','is','quadruped');
INSERT INTO "facts" VALUES('dave','is','man');
INSERT INTO "facts" VALUES('dave','likes','apple');
INSERT INTO "facts" VALUES('dave','knows','herman');
INSERT INTO "facts" VALUES('herman','knows','dave');
INSERT INTO "facts" VALUES('herman','is','man');
INSERT INTO "facts" VALUES('herman','knows','eric');
INSERT INTO "facts" VALUES('eric','knows','herman');
INSERT INTO "facts" VALUES('eric','is','man');
INSERT INTO "facts" VALUES('eric','knows','dave');
INSERT INTO "facts" VALUES('dave','knows','eric');
INSERT INTO "facts" VALUES('eric','drinks','raven_inn');
INSERT INTO "facts" VALUES('raven_inn','is','public_house');
INSERT INTO "facts" VALUES('mechanics_arms','is','public_house');
INSERT INTO "facts" VALUES('white_bear','is','public_house');
INSERT INTO "facts" VALUES('apple','is','fruit');
INSERT INTO "facts" VALUES('pear','is','fruit');
INSERT INTO "facts" VALUES('banana','is','fruit');
INSERT INTO "facts" VALUES('fruit','is','plant_seedcase');
INSERT INTO "facts" VALUES('edible_fruit','is','fruit');
INSERT INTO "facts" VALUES('edible_fruit','is','edible');
INSERT INTO "facts" VALUES('inedible_fruit','is','fruit');
INSERT INTO "facts" VALUES('inedible_fruit','is','inedible');
INSERT INTO "facts" VALUES('inedible','is','poison');
INSERT INTO "facts" VALUES('edible','is','nutritious');
INSERT INTO "facts" VALUES('patrick','is','man');
INSERT INTO "facts" VALUES('patrick','knows','eric');
INSERT INTO "facts" VALUES('eric','knows','patrick');
INSERT INTO "facts" VALUES('emily','is','girl');
INSERT INTO "facts" VALUES('dave','knows','emily');
INSERT INTO "facts" VALUES('emily','knows','dave');

COMMIT;
