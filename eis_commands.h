// used to enumerate the commands in the parser
#define	COMMAND_TRON			0
#define	COMMAND_TROFF			1
#define	COMMAND_ABOUT			2
#define	COMMAND_OBJECT		3
#define	COMMAND_PROPERTY	4
#define	COMMAND_BINDING		5
#define	COMMAND_HELP			6
#define COMMAND_DEPTH			7
#define COMMAND_SAVE			8
#define COMMAND_FACTS			9
#define COMMAND_FOL				10
#define COMMAND_QUERY			11
#define COMMAND_EXIT			12
#define COMMAND_COUNT			13
#define COMMAND_CONFLUX		14
#define COMMAND_HISTORY		15
#define COMMAND_REDO			16

// command structure
struct dotComm {

	// command name
	char *Command;

	// command abbreviation
	char *Abbrev;

	// help line 1
	char *Help1;

	// help line 2
	char *Help2;

	// usage
	char *Usage;

	// ID number of the command
	int nCommand;
};

// define an array of command structures
struct dotComm dCommands[] = {

	{
		".tron",
		".tron",
		".tron",
		"Turn system trace ON",
		".tron",
		COMMAND_TRON
	},

	{
		".troff",
		".troff",
		".troff",
		"Turn system trace OFF",
		".troff",
		COMMAND_TROFF
	},

	{
		".about",
		".a",
		".about",
		"Show About dialog (also .a)",
		".about",
		COMMAND_ABOUT
	},

	{
		".object",
		".o",
		".object [object [depth]]",
		"Perform object-centric search (also .o)",
		".object [object [depth]]",
		COMMAND_OBJECT
	},

	{
		".property",
		".p",
		".property [property [depth]]",
		"Perform property-centric search (also .p)",
		".property [property [depth]]",
		COMMAND_PROPERTY
	},

	{
		".binding",
		".b",
		".binding binding",
		"Perform binding-centric search (also .b)",
		".binding",
		COMMAND_BINDING
	},

	{
		".help",
		"?",
		".help",
		"Display this help information (also ?)",
		".help",
		COMMAND_HELP
	},

	{
		".depth",
		".d",
		".depth [depth]",
		"Set the default search depth (also .d)",
		".depth [depth]",
		COMMAND_DEPTH
	},

	{
		".save",
		".s",
		".save",
		"Save the current session as a text file (also .s)",
		".save",
		COMMAND_SAVE
	},

	{
		".facts",
		".f",
		".facts",
		"List all of the facts from the KnowledgeBase (also .f)",
		".facts",
		COMMAND_FACTS
	},

	{
		".fol",
		".fol",
		".fol",
		"List all of the facts from the KnowledgeBase as FOL predicates",
		".fol",
		COMMAND_FOL
	},

	{
		".query",
		".q",
		".query object [property]",
		"Determine whether <object> has any links to <property> (also .q)",
		".query [object [property]]",
		COMMAND_QUERY
	},

	{
		".exit",
		".x",
		".exit",
		"Exit from the program (also .x)",
		".exit",
		COMMAND_EXIT
	},

	{
		".count",
		".c",
		".count",
		"Count the facts in knowledge base (also .c)",
		".count",
		COMMAND_COUNT
	},

	{
		".conflux",
		".cx",
		".conflux object1 object2",
		"Determine the confluence between object1 and object2 (also .co)",
		".conflux object1 object2",
		COMMAND_CONFLUX
	},

	{
		".history",
		"#",
		".history",
		"List command history (also #)",
		".history",
		COMMAND_HISTORY
	},

	{
		".redo",
		"!",
		".redo",
		"Redo a command from the history list (also !)",
		".redo <command number>",
		COMMAND_REDO
	},

	{
		NULL,
		NULL,
		NULL,
		NULL,
		NULL,
		(-1)
	}

};
